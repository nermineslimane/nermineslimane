### Hi there 👋

- 🔭 I’m currently working on Cynoia platform
- 🌱 I’m currently learning Backend technologies
- 💬 Ask me about Backend technos such as NodeJS,  express, Sequelize, AWS s3 , Google APIs, Dropbox API ...
- 📫 How to reach me: nermineslimane@gmail.com

    ![Anurag's GitHub stats](https://github-readme-stats.vercel.app/api?username=nermineslimane&show_icons=true&theme=radical&count_private=true)


    [![willianrod's wakatime stats](https://github-readme-stats.vercel.app/api/wakatime?username=nermineslimane)](https://github.com/anuraghazra/github-readme-stats)




<!--
**nermineslimane/nermineslimane** is a ✨ _special_ ✨ repository because its `README.md` (this file) appears on your GitHub profile.

Here are some ideas to get you started:

- 🔭 I’m currently working on Cynoia platform
- 🌱 I’m currently learning Backend technologies
- 👯 I’m looking to collaborate on ...
- 💬 Ask me about NodeJS,  express, Sequelize, AWS s3 , Google APIs, Dropbox API ...
- 📫 How to reach me: nermineslimane@gmail.com

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=nermineslimane&layout=compact)](https://github.com/anuraghazra/github-readme-stats)
[![willianrod's wakatime stats](https://github-readme-stats.vercel.app/api/wakatime?username=nermineslimane)](https://github.com/anuraghazra/github-readme-stats)

-->
